<?php
/**
 * Implements hook_form_system_theme_settings_alter() function.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function zen_base_theme_form_system_theme_settings_alter(&$form, $form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }
  $form['main_menu_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Main Menu Settings'),
  );
  $kci_main_menu_dropdown_enabled = theme_get_setting('kci_main_menu_dropdown_enabled');
  if (empty($kci_main_menu_dropdown_enabled)) {
    $kci_main_menu_dropdown_enabled = 0;
  }
  $form['main_menu_settings']['kci_main_menu_dropdown_enabled'] = array(
    '#type' => 'radios',
    '#title' => t('Enable Drop-down Functionality'),
    '#description' => t('Select this to make the menu function as a drop-down, with click-to-open functionality. NOTE: This will NOT apply any CSS. You must still apply the appropriate drop-down CSS to make it work. You can use the drop-down-menu-with-click mixin included with the base theme - see _mixins.sass. The drop-down functionality will apply a class of "opened" when a top-level item with a drop-down is clicked on.'),
    '#options' => array(
      0 => 'No',
      1 => 'Yes',
    ),
    '#required' => TRUE,
    '#default_value' => $kci_main_menu_dropdown_enabled,
  );
  $kci_main_menu_dropdown_selector = theme_get_setting('kci_main_menu_dropdown_target_selector');
  $form['main_menu_settings']['kci_main_menu_dropdown_target_selector'] = array(
    '#type' => 'textfield',
    '#title' => t('Menu Selector') . ' <span class="form-required fa fa-star" title="' . t('This field is required') . '">*</span>',
    '#description' => t('Specify the CSS selector to target the main menu'),
    '#default_value' => $kci_main_menu_dropdown_selector,
    '#states' => array(
      'visible' => array(
        ':input[name="kci_main_menu_dropdown_enabled"]' => array('value' => 1),
      ),
    ),
    '#required' => FALSE,
    '#element_validate' => array('dropdown_selector_field_validate'),
  );
  // Create the form using Forms API
  $form['metatags'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Metatag Settings'),
    '#description'   => t('These settings are provided for basic meta tag control if you are not using the Metatags module. Note that settings and overrides for the site title ONLY affect the title metatag.'),
  );
  $title_elements = theme_get_setting('kci_site_title_elements');
  if (empty($title_elements)) {
    $title_elements = array('title', 'name', 'slogan');
  }
  $form['metatags']['kci_site_title_elements'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Site title elements'),
    '#description'   => t('Select the elements you want to be included in the site title. Since the site title is required, if none are selected it will revert to the default, which is to include all three.'),
    '#default_value' => $title_elements,
    '#options'       => array(
                          'title'  => t('Page Title (not applicable on home page)'),
                          'name'   => t('Site name'),
                          'slogan' => t('Site slogan (only applicable on home page)'),
                        ),
  );
  $form['metatags']['kci_site_title_name_override'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Site title name override'),
    '#description'   => t('Use this as the site name in the title tag for all pages, ignoring the default site title'),
    '#default_value' => theme_get_setting('kci_site_title_name_override'),
  );
  $form['metatags']['kci_site_title_name_home_override'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Home site title name override'),
    '#description'   => t('Use this as the site name in the title tag for the home page, ignoring the default site title'),
    '#default_value' => theme_get_setting('kci_site_title_name_home_override'),
  );
  $form['metatags']['kci_site_title_slogan_override'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Site title slogan override'),
    '#description'   => t('If the site slogan you have set is not really appropriate for use in the site title and you want something different instead, override it here.'),
    '#default_value' => theme_get_setting('kci_site_title_slogan_override'),
  );
  $form['metatags']['kci_site_title_separator'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Site title separator'),
    '#description'   => t('Character to separate the elements of the site title. Defaults to pipe (|) if left blank. One space will automatically be placed on either side of this character.'),
    '#default_value' => theme_get_setting('kci_site_title_separator'),
    '#size'          => 4,
  );
  $form['metatags']['kci_enabled_og_metatags'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('OpenGraph Metatags'),
    '#description'   => t('Select the OpenGraph metatags you would like to include. Select none if you plan to use the Metatags module.'),
    '#default_value' => theme_get_setting('kci_enabled_og_metatags'),
    '#options'       => array(
                          'title'  => t('Title (will be the same as the site title per settings above)'),
                          'url'    => t('URL'),
                          'image'  => t('Image (will look for an image file named "og-preview.(gif|png|jpg)" in the theme\'s image folder)'),
                        ),
  );
}

/**
 * Validate the drop-down menu selector field.
 */
function dropdown_selector_field_validate($element, &$form_state, $form) {
  if (!empty($form_state['values']['kci_main_menu_dropdown_enabled']) && empty($element['#value'])) {
    form_error($element, t('!name field is required', array('!name' => t('Menu Selector'))));
  }
}
