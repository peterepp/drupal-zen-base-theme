<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728096
 */

/**
 * Pre-render components for the page template.
 *
 * @param $variables
 *    An array of variables to pass to the theme template.
 */
function zen_base_theme_preprocess_page(&$variables) {
  $variables['secondary_menu'] = theme('links__system_secondary_menu', array(
    'links' => $variables['secondary_menu'],
    'attributes' => array(
      'class' => array('menu', 'inline', 'clearfix'),
    ),
  ));
  $variables['top_bar'] = render($variables['page']['top_bar']);
  $variables['banner'] = render($variables['page']['banner']);
  $variables['sidebar_first']  = render($variables['page']['sidebar_first']);
  $variables['sidebar_second'] = render($variables['page']['sidebar_second']);
  $variables['main_menu'] = theme('links__system_main_menu', array(
    'links' => $variables['main_menu'],
    'attributes' => array(
    'class' => array('menu', 'clearfix'),
    ),
  ));
  $variables['navigation'] = render($variables['page']['navigation']);
  $variables['footer'] = render($variables['page']['footer']);
}

/**
 * Override or insert variables into the html templates. We use this to apply some of our meta tag customisations
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function zen_base_theme_preprocess_html(&$variables, $hook) {
  drupal_add_js(drupal_get_path('theme', 'zen_base_theme') . '/js/file-input.js', array('scope' => 'footer'));
  $kci_main_menu_dropdown_enabled = theme_get_setting('kci_main_menu_dropdown_enabled');
  if ($kci_main_menu_dropdown_enabled) {
    drupal_add_js(drupal_get_path('theme', 'zen_base_theme') . '/js/dropdown-menu.js', array('scope' => 'footer'));
    drupal_add_js(array('kci_main_menu_dropdown_target_selector' => theme_get_setting('kci_main_menu_dropdown_target_selector')), 'setting');
  }
  // Site title customisations, if any:
  $site_title_elements      = theme_get_setting('kci_site_title_elements');
  $site_title_override      = theme_get_setting('kci_site_title_name_override');
  $site_title_home_override = theme_get_setting('kci_site_title_name_home_override');
  $site_slogan_override     = theme_get_setting('kci_site_title_slogan_override');
  $site_title_separator     = trim(theme_get_setting('kci_site_title_separator'));
  if (!empty($site_title_separator)) {
    $site_title_separator = ' ' . $site_title_separator . ' ';
  } else {
    $site_title_separator = ' | ';
  }
  if (!empty($site_title_elements)) {
    // Replace the head title array with only the elements we want to keep:
    $head_title_array = array();
    foreach ($site_title_elements as $element_name) {
      if (!empty($variables['head_title_array'][$element_name])) {
        $head_title_array[$element_name] = $variables['head_title_array'][$element_name];
      }
    }
    $variables['head_title_array'] = $head_title_array;
  }
  if (drupal_is_front_page()) {
    if (!empty($site_title_home_override) && !empty($variables['head_title_array']['name'])) {
      $variables['head_title_array']['name'] = $site_title_home_override;
    }
  } else {
    if (!empty($site_title_override) && !empty($variables['head_title_array']['name'])) {
      $variables['head_title_array']['name'] = $site_title_override;
    }
  }
  if (!empty($site_slogan_override) && !empty($variables['head_title_array']['slogan'])) {
    $variables['head_title_array']['slogan'] = $site_slogan_override;
  }
  $variables['head_title'] = implode($site_title_separator, $variables['head_title_array']);
  // OpenGraph metatags:
  $enabled_og_tags = theme_get_setting('kci_enabled_og_metatags');
  if (!empty($enabled_og_tags)) {
    if (in_array('title', $enabled_og_tags)) {
      $variables['kci_og_tag_title'] = $variables['head_title'];
    }
    if (in_array('url', $enabled_og_tags)) {
      $path_alias = drupal_get_path_alias();
      if ($path_alias == 'node') {
        $path_alias = '';
      }
      $variables['kci_og_tag_url'] = url($path_alias, array('absolute' => true));
    }
    if (in_array('image', $enabled_og_tags)) {
      global $theme;
      $path = drupal_get_path('theme', $theme);
      $og_image_paths = array(
        $path . '/images/og-preview.jpg',
        $path . '/images/og-preview.jpeg',
        $path . '/images/og-preview.gif',
        $path . '/images/og-preview.png',
      );
      $og_image_path = '';
      foreach ($og_image_paths as $image_path) {
        if (file_exists(DRUPAL_ROOT . '/' . $image_path)) {
          $og_image_path = $image_path;
          break;
        }
      }
      if (!empty($og_image_path)) {
        $variables['kci_og_tag_image'] = url($og_image_path, array('absolute' => true));
      }
    }
  }
}

/**
 * Returns HTML for primary and secondary local tasks.
 *
 * @ingroup themeable
 */
function zen_base_theme_menu_local_tasks(&$variables) {
  $output = '';

  // Add theme hook suggestions for tab type.
  foreach (array('primary', 'secondary') as $type) {
    if (!empty($variables[$type])) {
      foreach (array_keys($variables[$type]) as $key) {
        if (isset($variables[$type][$key]['#theme']) && ($variables[$type][$key]['#theme'] == 'menu_local_task' || is_array($variables[$type][$key]['#theme']) && in_array('menu_local_task', $variables[$type][$key]['#theme']))) {
          $variables[$type][$key]['#theme'] = array('menu_local_task__' . $type, 'menu_local_task');
        }
      }
    }
  }

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="tabs-primary tabs primary clearfix">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs-secondary tabs secondary clearfix">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}

/**
 * Implements theme_menu_tree().
 *
 * Adds a clearfix class to all menu trees.
 */
function zen_base_theme_menu_tree($variables) {
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_status_messages().
 *
 * Customise output of status messages to include close buttons.
 */
function zen_base_theme_status_messages($variables) {
  $display = $variables['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
  );
  foreach (drupal_get_messages($display) as $type => $messages) {
    $output .= "<div class=\"messages--$type messages $type\">\n";
    if (!empty($status_heading[$type])) {
      $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>\n";
    }
    if (count($messages) > 1) {
      $output .= " <ul class=\"messages__list\">\n";
      foreach ($messages as $message) {
        $output .= '  <li class=\"messages__item\">' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= $messages[0];
    }
    $output .= '<a class="message-close-button fa fa-times-circle" href="#close-messages-' . $type . '">' . t('Close') . '</a>';
    $output .= "</div>\n";
  }
  drupal_add_js(drupal_get_path('theme', 'zen_base_theme') . '/js/messages.js', array('scope' => 'footer'));
  return $output;
}

/**
 * Implements theme_tablesort_indicator().
 *
 * Use fontawesome for asc/desc tablesort indicator.
 */
function zen_base_theme_tablesort_indicator($variables) {
  $style = $variables['style'];
  $theme_path = drupal_get_path('theme', 'adminimal');
  if ($style == 'asc') {
    return '<i class="fa fa-caret-down" aria-hidden="true"></i>';
  }
  else {
    return '<i class="fa fa-caret-up" aria-hidden="true"></i>';
  }
}

/**
 * Implements theme_admin_block().
 * Adding classes to the administration blocks see issue #1869690.
 */
function zen_base_theme_admin_block($variables) {
  $block = $variables['block'];
  $output = '';

  // Don't display the block if it has no content to display.
  if (empty($block['show'])) {
    return $output;
  }

  $icon_code = '';
  if (!empty($block['path'])) {
    $classes = explode(' ', check_plain(str_replace("/", " ", $block['path'])));
    $icon_code = zen_base_theme_get_admin_icon_code(end($classes));
    $output .= '<div class="admin-panel ' . implode(' ', $classes) . ' ">' . $icon_code;
  }
  elseif (!empty($block['title'])) {
    $output .= '<div class="admin-panel ' . check_plain(strtolower($block['title'])) . '">';
  }
  else {
    $output .= '<div class="admin-panel">';
  }

  if (!empty($block['title'])) {
    $output .= '<h3 class="title">' . $block['title'] . '</h3>';
  }

  if (!empty($block['content'])) {
    $output .= '<div class="body">' . $block['content'] . '</div>';
  }
  else {
    $output .= '<div class="description">' . $block['description'] . '</div>';
  }

  $output .= '</div>';

  return $output;
}

/**
 * Return icon code for a given classname.
 */
function zen_base_theme_get_admin_icon_code($classname) {
  $icon_code = '';
  switch ($classname) {
    case "people":
      $icon_code = '<div class="admin-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-users fa-stack-1x fa-inverse"></i></span></div>';
      break;

    case "content":
      $icon_code = '<div class="admin-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></span></div>';
      break;

    case "date":
      $icon_code = '<div class="admin-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-calendar fa-stack-1x fa-inverse"></i></span></div>';
      break;

    case "media":
      $icon_code = '<div class="admin-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-file-image-o fa-stack-1x fa-inverse"></i></span></div>';
      break;

    case "search":
      $icon_code = '<div class="admin-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-search fa-stack-1x fa-inverse"></i></span></div>';
      break;

    case "regional":
      $icon_code = '<div class="admin-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-globe fa-stack-1x fa-inverse" aria-hidden="true"></i></span></div>';
      break;

    case "user-interface":
      $icon_code = '<div class="admin-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-hand-pointer-o fa-stack-1x fa-inverse"></i></span></div>';
      break;

    case "development":
      $icon_code = '<div class="admin-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-terminal fa-stack-1x fa-inverse"></i></span></div>';
      break;

    case "services":
      $icon_code = '<div class="admin-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-rss fa-stack-1x fa-inverse" aria-hidden="true"></i></span></div>';
      break;

    case "system":
      $icon_code = '<div class="admin-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-server fa-stack-1x fa-inverse"></i></span></div>';
      break;

    case "administration":
    default:
      $icon_code = '<div class="admin-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-cogs fa-stack-1x fa-inverse"></i></span></div>';
      break;

  }
  return $icon_code;
}

/**
 * Returns HTML for a button form element.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #attributes, #button_type, #name, #value.
 *
 * @ingroup themeable
 */
function zen_base_theme_button($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name', 'value'));

  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
  }
  if (preg_match('/download/i', $element['#attributes']['value'])) {
    $element['#attributes']['class'][] = 'download-button';
  }
  else if (preg_match('/edit/i', $element['#attributes']['value'])) {
    $element['#attributes']['class'][] = 'edit-button';
  }
  else if (preg_match('/search/i', $element['#attributes']['value'])) {
    $element['#attributes']['class'][] = 'search-button';
  }
  else if (preg_match('/add/i', $element['#attributes']['value'])) {
    $element['#attributes']['class'][] = 'add-button';
  }
  else if (preg_match('/delete/i', $element['#attributes']['value'])) {
    $element['#attributes']['class'][] = 'delete-button';
  }
  else if (preg_match('/cancel/i', $element['#attributes']['value'])) {
    $element['#attributes']['class'][] = 'cancel-button';
  }
  else if (preg_match('/undo|reset/i', $element['#attributes']['value'])) {
    $element['#attributes']['class'][] = 'reset-button';
  }
  else if (preg_match('/update|refresh/i', $element['#attributes']['value'])) {
    $element['#attributes']['class'][] = 'refresh-button';
  }
  else if (preg_match('/upload/i', $element['#attributes']['value'])) {
    $element['#attributes']['class'][] = 'upload-button';
  }
  else if (preg_match('/(login|log in)/i', $element['#attributes']['value'])) {
    $element['#attributes']['class'][] = 'login-button';
  }
  else if (preg_match('/(logout|log out)/i', $element['#attributes']['value'])) {
    $element['#attributes']['class'][] = 'logout-button';
  }
  else if (preg_match('/(filter)/i', $element['#attributes']['value'])) {
    $element['#attributes']['class'][] = 'filter-button';
  }
  else if (preg_match('/(pay)/i', $element['#attributes']['value'])) {
    $element['#attributes']['class'][] = 'pay-button';
  }
  else if (preg_match('/(next)/i', $element['#attributes']['value'])) {
    $element['#attributes']['class'][] = 'next-button';
  }
  else if (preg_match('/(previous|prev)/i', $element['#attributes']['value'])) {
    $element['#attributes']['class'][] = 'prev-button';
  }
  else if (preg_match('/(cart|checkout)/i', $element['#attributes']['value'])) {
    $element['#attributes']['class'][] = 'cart-button';
  }
  if ((!empty($element['#attributes']['id']) && preg_match('/^edit\-displays/i', $element['#attributes']['id'])) || in_array('add-display', $element['#attributes']['class'])) {
    // Render any views edit-displays buttons as regular inputs, otherwise they don't get properly turned
    // into the drop-down menu.
    return '<input' . drupal_attributes($element['#attributes']) . ' />';
  }
  else {
    // Otherwise render all buttons as a <button> HTML element to allow for icons to be displayed.
    $text_label = check_plain($element['#attributes']['value']);
    return '<button' . drupal_attributes($element['#attributes']) . '>' . $text_label . '</button>';
  }
}

/**
 * Implements hook_media_wysiwyg_token_to_markup_alter().
 */
function zen_base_theme_media_wysiwyg_token_to_markup_alter(&$element, $tag_info, $settings) {
  if (isset($element['content']['#bundle']) && $element['content']['#bundle'] == 'image') {
    $element['content']['#type'] = 'markup';
    $element_attributes = '';
    if (!empty($element['content']['#attributes'])) {
      $element_attributes = drupal_attributes($element['content']['#attributes']);
    }
    $element['content']['#prefix'] = '<span ' . $element_attributes . '>';
    $element['content']['#suffix'] = '</span>';
  }
}
