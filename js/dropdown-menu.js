(function ($, Drupal, window, document, undefined) {

// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.dropdown_menu = {
  attach: function(context, settings) {
    $(settings.kci_main_menu_dropdown_target_selector + ' > li').each(function(index) {
      if ($(this).find('ul.menu').length) {
        $(this).find('> a, > span.nolink').on('click touchstart', function(event) {
          event.preventDefault();
          $(settings.kci_main_menu_dropdown_target_selector + ' li.opened').not($(this).parents('li')).removeClass('opened');
          $(this).parent('li').toggleClass('opened');
          return false;
        });
      }
    });
    $(document).click(function(event) {
      if ($(event.target).closest($('.menu__link, .menu__item')).length === 0) {
        $(settings.kci_main_menu_dropdown_target_selector + ' li.opened').removeClass('opened');
      }
    });
    if ($.isFunction($.fn.on)) {
      $(document).on('touchstart', function(event) {
        if ($(event.target).closest($('.menu__link, .menu__item')).length === 0) {
          $(settings.kci_main_menu_dropdown_target_selector + ' li.opened').removeClass('opened');
        }
      });
    }
  }
};

})(jQuery, Drupal, this, this.document);
