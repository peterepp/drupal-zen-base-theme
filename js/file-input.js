(function ($, Drupal, window, document, undefined) {

// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.file_inputs = {
  attach: function(context, settings) {

    $('input[type=file], input[type=image]').once('file-input-select', function() {
      if ($(this).closest('#media-browser-page').length == 0) {
        var label = $(this).closest('.form-item').find('label');
        label.addClass('file-select-button');
        $(this).change(function() {
          var label_value = '';
          var numFiles = $(this)[0].files.length;
          if (numFiles > 1) {
            label_value = Drupal.t('@count Files Selected', {'@type': numFiles});
          }
          else {
            label_value = $(this).val().split('\\').pop();
          }
          label.html(label_value).addClass('file-selected');
        });
      }
    });

  }
};

})(jQuery, Drupal, this, this.document);
