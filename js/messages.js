(function ($, Drupal, window, document, undefined) {

// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.message_controls = {
  attach: function(context, settings) {

    $('.message-close-button').once(function() {
      $(this).click(function(event) {
        event.preventDefault();
        var messages_container = $(this).parent().parent('.container');
        $(this).parent().slideUp('fast', function() {
          $(this).remove();
          if (!messages_container.children('.messages').length) {
            messages_container.remove();
          }
        });
        return false;
      });
    });
  }
};

})(jQuery, Drupal, this, this.document);
