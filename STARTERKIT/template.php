<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728096
 */


/**
 * Override or insert variables into the maintenance page template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_maintenance_page(&$variables, $hook) {
  // When a variable is manipulated or added in preprocess_html or
  // preprocess_page, that same work is probably needed for the maintenance page
  // as well, so we can just re-use those functions to do that work here.
  STARTERKIT_preprocess_html($variables, $hook);
  STARTERKIT_preprocess_page($variables, $hook);
}
// */

/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_html(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // The body tag's classes are controlled by the $classes_array variable. To
  // remove a class from $classes_array, use array_diff().
  //$variables['classes_array'] = array_diff($variables['classes_array'], array('class-to-remove'));
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_page(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_node(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // Optionally, run node-type-specific preprocess functions, like
  // STARTERKIT_preprocess_node_page() or STARTERKIT_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $variables['node']->type;
  if (function_exists($function)) {
    $function($variables, $hook);
  }
}
// */

/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_comment(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the region templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("region" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_region(&$variables, $hook) {
  // Don't use Zen's region--sidebar.tpl.php template for sidebars.
  //if (strpos($variables['region'], 'sidebar_') === 0) {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('region__sidebar'));
  //}
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_block(&$variables, $hook) {
  // Add a count to all the blocks in the region.
  // $variables['classes_array'][] = 'count-' . $variables['block_id'];

  // By default, Zen will use the block--no-wrapper.tpl.php for the main
  // content. This optional bit of code undoes that:
  //if ($variables['block_html_id'] == 'block-system-main') {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('block__no_wrapper'));
  //}
}
// */

/**
 * Implements theme_menu_link().
 *
 * Adds fontawesome icons to expanded menu items as well as
 * wraps the item text in a span to help with styling.
 */
/* -- Delete tis line if you want to use this function
function STARTERKIT_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  // Keep track of depth so we can add the appropriate
  // icon to expanded menu items.
  if (empty($element['#depth'])) {
    $depth = 1;
  }
  else {
    $depth = $element['#depth'];
  }

  if (!empty($element['#below'])) {
    foreach (element_children($element['#below']) as $key) {
      $element['#below'][$key]['#depth'] = $depth + 1;
    }
    $sub_menu = drupal_render($element['#below']);
  }
  $element['#localized_options']['html'] = TRUE;
  $link_text_extra = '';
  if (!empty($element['#attributes']['class']) && in_array('is-expanded', $element['#attributes']['class'])) {
    $icon = 'caret-down';
    if ($depth > 1) {
      $icon = 'caret-right';
    }
    $link_text_extra = '<i class="indicator-icon fa fa-' . $icon . '"></i>';
    $element['#localized_options']['attributes']['class'][] = 'with-icon';
  }
  $output = l('<span class="link-text">' . $element['#title'] . $link_text_extra . '</span>', $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}
// */

/**
 * Implements theme_menu_link().
 *
 * Adds fontawesome icons to expanded menu items as well as
 * wraps the item text in a span to help with styling.
 */
function STARTERKIT_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  // Keep track of depth so we can add the appropriate
  // icon to expanded menu items.
  if (empty($element['#depth'])) {
    $depth = 1;
  }
  else {
    $depth = $element['#depth'];
  }

  if (!empty($element['#below'])) {
    foreach (element_children($element['#below']) as $key) {
      $element['#below'][$key]['#depth'] = $depth + 1;
    }
    $sub_menu = drupal_render($element['#below']);
  }
  $element['#localized_options']['html'] = TRUE;
  $link_text_extra = '';
  $kci_main_menu_dropdown_enabled = theme_get_setting('kci_main_menu_dropdown_enabled');
  if ($kci_main_menu_dropdown_enabled && !empty($element['#attributes']['class']) && in_array('is-expanded', $element['#attributes']['class'])) {
    $link_text_extra .= '<i class="indicator-icon fa fa-caret-down"></i>';
    $element['#localized_options']['attributes']['class'][] = 'with-icon';
  }
  if (!empty($element['#localized_options']['attributes']['class']) && in_array('home-icon', $element['#localized_options']['attributes']['class'])) {
    $link_text_extra .= '<i class="fa fa-home"></i>';
    $element['#localized_options']['attributes']['title'] = $element['#title'];
  }
  $output = l('<span class="link-text">' . $element['#title'] . $link_text_extra . '</span>', $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}
// */
