## Welcome to the Zen Base Theme Kit ##

This theme is a sub-theme of Zen, designed to be used as a base theme for your site with Zen as the ultimate parent theme. The purpose of this is to provide much more complete starter kit, with a more robust set of base styling that allows for much more rapid development of a new theme in your site.

It is very bare bones to begin with, providing by default a responsive layout and strong set of CSS with variables that allow you to change a large amount of the theme style without writing a single line of CSS code. When you build a new site, begin by copying the STARTERKIT folder from this theme into your sites/all/themes folder and renaming it, and it's .info.txt file, appropriately. Enable that theme and you're ready to begin.

This version of the theme currently depends on Zen theme version 5.x. The plan is to upgrade it to work with the latest version of the Zen theme and I welcome any contributors who wish to put the effort into helping with that.

## Working with the SASS Files ##

You will want to use compass to compile your CSS, and this version of the theme requires older versions of compass and other libraries. As such, you need to use bundler to install and use compass and other required libraries. From within the theme folder, run "bundler update" to fetch all the required libraries and binaries. Then, you can run "bundler exec compass compile" or "bundler exec compass watch" during development.

### SASS File Organisation ###

Just as with the original Zen starterkit, SASS files are organised in a way that is designed to follow SMACSS standards. The zen_base_theme folder contains a sass folder with common styling that should always be used and often do not need to be overridden in a site's sub theme. It includes some base print styles, normalisation styles, and some helpful mixins (documented below). It also include styles for many core Drupal components within it's components folder. Any of these files can be copied into the same folder in the sub-theme if you want to override them. When compiling the SASS, any files that need to be imported, but are not included in the sub theme, will be included from the base theme.

Here is a breakdown of all the sass files that come with the theme.

#### Files Included in STARTERKIT Sub-theme ####

`styles.sass`  
This is the core stylesheet and the only one that actually gets compiled into a CSS file. It has @import statements for including all other sass files needed to create the theme for a site.

`_init.sass`  
This file imports some of the core required base components and sets up ALL the variables for use throughout the theme. See the documentation within this file for explanations of each of the variables. This is your primary starting point for customising your theme. Before you write any new SASS code, you'll want to customise the variables in this file, which setup the layout and most of the branding colours for the site.

`_type.sass`  
This file is for configuring all the font and type settings for your theme. After the `_init.sass` file, this is the second one you should customise prior to writing an SASS code. If you want to load any custom fonts from external sources such as Google fonts, or fonts you install locally in your theme, you should import that CSS here. You can configure all base font sizes and default font families in this file. Add any additional variables you need to use in your theme related to typeface in this file.

`_layout.sass`  
This file defines the base layout structure of your site using the Zen grid system. It is important to understand that the Zen grid system is not meant or designed to function like other grid systems, such as bootstrap. It's purpose is not to create a grid that dynamically wraps items to the next line and adjusts the number of columns across the screen automatically like other grid systems do. Do not even try to use zen grids for that purpose or in that way. Instead, it is desgined to place elements on a page starting in a given column spanning a given number of columns regardless of what columns come before or after it. You can, for example, place an element in the middle of a page without any other columns before or after it and it will always stay in that position. It is necessary to use media queries to redefine the starting position and column span for the same elements for each screen size, unlike other responsive grids that just dynamically adapt without media queries. This system works very well for it's intended purpose.

In this file, you can not only define the layout of the 3 main columns in the content area of the template (2 sidebars and a content area), but also the navigation and footer layouts. This file should ONLY be used for defining structure and layout. Styling of any of the elements in this file should be done elsewhere. See below under "Content First Approach" for more details on how the zen based layout system works.

There is also a `_layout-rtl.sass` file you can explore and use if you are build a site that uses a right-to-left font and layout.

`_theme.sass`  
This file is the one in which you should put all common styling code. While you can also use it to put all the CSS to style specific pages, features and components of your site, they are better to separate out into individual files within the "components" folder. We recommend using this file for only the most generic styles and common SMACSS-style module rules.

`_font-awesome.min.scss`  
This theme includes a copy of the font awesome icon font, so this just provides the CSS for that font. No need to touch the contents of this file, unless you update your theme to a newer version of font awesome.

##### The `components` Folder #####

This is where styling for various individual specific components of the site should go. In here you will find by default the following files:

* `_footer.sass` - styling for the footer element and it's content
* `_header.sass` - styling for the header element and it's content
* `_navigation.sass` - styling for menus and any other navigation elements of the site

These three are included within the STARTERKIT because they almost always need to be populated with custom styles specific to the site, but there are many other files in the base theme that will be used if not copied into the sub-theme to override them.

#### Files Included in the Base Theme ####

The base theme includes some base sass files that most commonly can stay as-is, including:

`_print.sass`  
Basic print stylesheet that hides most elements we don't want to print (header, footer, sidebars, menus, etc). It ensures that the content area fits to the width of the paper, and it adds visible URLs shown after hyperlinks.

`_normalize.sass`  
Standard CSS for normalizing many HTML elements and font sizes across browsers.

`_mixins.sass`  
A set of helpful mixins you can use in your site if desired. These are documented in more detail below.

##### The `components` Folder #####

As noted above, this folder contains many files for styling of common Drupal site components. For the most part, you shouldn't need to override most of these in your sub-theme because they make use of the variables in the `_init.sass` file in order to easily customise them. Here is a breakdown of each one:

`_anchors.sass`  
Site-wide anchor styles. Uses colour variables from the `_init.sass` file and sets up smooth transition of the colours on hover, for browsers that support CSS3 transitions.

`_blocks.sass`  
Puts a default bottom margin on all Drupal blocks using the $base-leading variable.

`_breadcrumb.sass`  
Very basic styling of breadcrumbs, to ensure they display inline, have appropriate padding and the right font size. Uses variables from `_init.sass`.

`_contextual-links.sass`  
Custom styling of Drupal contextual links. Uses variables from `_init.sass` for customisation. Should never need to be overriden.

`_drupal.sass`  
Styling of a bunch of Drupal core components, including:

* Action links
* Fieldset legends
* Comments
* Messages
* More Links
* Unpublished indicator
* Other small miscelaneous items

`_forms.sass`  
Styling for all form components in a site. Covers all form components, including the Chosen select widget if used in the site. It will style submit, vs delete vs other regular form submit buttons distinctly. The majority of form styling can be customised with variables in the `_init.sass`, but you may find a need to copy this one into your sub-theme to override it sometimes.

`_misc.sass`  
Styling for various miscelaneous components - review the file in detail to see. One thing of note is that this file provides some improved layout styling for the password form components, like the strength meter, to make them properly responsive out of the box.

`_misc-rtl.sass`  
Styling of various miscelaneous components for right-to-left layouts.

`_pager.sass`  
Most basic styling and layout for pager elements.

`_sidebar.sass`  
Nothing here at the moment, reserved for possible future use.

`_tables.sass`  
Very basic common table styles, including odd/even colour striping and draggable styles.

`_tabs.sass`  
Styling for the Drupal tabs. Fully customisable using variables in the `_init.sass` file.

#### SASS Mixins ####

This is a quick overview of available mixins so you are aware of what's available. Review the mixins sass file for more details on how to use them.

`element-invisible`  
Used to hide elements that exist primarily for accessibility purposes but don't need to be displayed. This already gets applied to any elements with the element-invisible class on them. You probably don't need to use this in your theme. This has some related mixins you can read about by looking in the mixins file.

`views-row-common-style`  
Some common styling you can apply to view rows that puts some spacing and a border between them.

`zen-clearfix`  
When you can't add the "clearfix" class to something, but need that same kind of proper clearfix becaues the standard clearfix mixin is no good (all that one does is set overflow to hidden).

`block-breadcrumbs`  
Nice block-style breadcrumbs that makes the end of each breadcrumb arrow-shaped so there's no need for a breadcrumb separator character. Make sure if you use this that you edit the theme settings and remove the breadcrumb separator. Other modules, such as path breadcrumbs, may also override the theme settings, so if you use such a module be sure to check it's settings to ensure it's not providing a breadcrumb separator.

`vertical-center-container`, `vertical-center` and `horizontal-center`  
Uses a little CSS trickery to center things both horizontally and vertically that are otherwise hard to center.

`vertical-center-container` can be applied to the container of the element you want to vertically center. It sets its position to relative and gives it a fixed height. This is not always required, if the containing element is already positioned relative or absolute. The fixed height is also not always needed.

`vertical-center` actually applies the vertical centering, and works as long as the container is positioned relative or absolute as well.

`horizontal-center` does the same thing as vertical center so long as the containing element is positioned relative or absolute.

`drop-down-menu`  
Move over nice menus or other Javascript solutions. You can apply a drop-down (or fly-out) menu really easily with this mixin. Apply it to the UL element of your menu with the appropriate parameters and that's it. Be sure to apply it only to the right screen sizes where drop-downs can be used by wrapping it in a media selector. It won't automatically be disabled on mobile devices.
